import fabric
from fabric.config import Config
import yaml
from pathlib import Path
import logging
from remote_host_manipulator2.parser.parse import parse
from multiprocessing import Process


COMMAND_YAML_DIR = Path(__file__).parents[1] / 'commands'


class Linux():
    def __init__(self, host, user, password, port=22):
        self.config = Config(overrides={
            'port': port,
            'user': user,
            'connect_kwargs': {
                'password': password
            },
            'system_ssh_path': '/etc/ssh/ssh_config',
            'user_ssh_path': '~/.ssh/config',
            'inline_ssh_env': True,
            "run": {
                "warn": True,
                "env": {
                    # HACK: PATH environment variables is not auto set.
                    #       because, command via fabric2 is executed in not interactive shell.
                    #       so, /etc/profile isn't read
                    "PATH": "/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin"
                },
                "hide": True,
                "pty": True
            }
        },
            lazy=True
        )

        # 出力は全てloggingを経由させます
        logging.basicConfig()
        self.logger = logging.getLogger(
            "remote_host_manipulator." + self.__class__.__name__)
        self.logger.setLevel(logging.INFO)

        self.c_auth = fabric.Connection(host=host, config=self.config)

        # バックグラウンドで実行しているプロセスを覚えておくための辞書です。
        # バックグラウンドで実行する際にはプロセスを一意に識別するための名前 ( `label` )を指定して、プロセスを覚えさせます。
        # {label: Process()}
        self.processes = {}

    def run(self, command, sudo=False):
        '''
        コマンドを実行します
        '''

        # _add_command_prefixでコマンドのプレフィックスを付けます、sudoなど
        # 継承した時に_add_command_prefixを変更することで、ESEやnamespaceが対象でも変更が少なく済みます
        # ese.pyやnamespace.pyを参照してみてください
        command = self._add_command_prefix(command, sudo)
        # 実行するコマンドを出力します
        self.logger.info(command)
        result = self.c_auth.run(command)

        # 結果を出力します
        self.logger.info(result)
        return result

    def run_background(self, label, command, sudo=False):
        '''
        バックグラウンドでコマンドを実行します
        '''

        if label in self.processes:
            # TODO: もうプロセスが終わっている場合は消す処理を入れる
            raise RuntimeError('label "{}" already existed')
        command = self._add_command_prefix(command, sudo)
        self.logger.info(command)
        # 元のremote_host_manipulatorと同様にmultiprocessingを使用してバックグラウンドでの実行を実現します
        # 2020/01の初旬にinvoke.runに追加された `asynchronous` オプションを使用すれば (fabricでも使用できれば、未確認) より簡潔に実装できるかもしれません
        p = Process(target=self._process_target, args=(command,))
        p.start()
        # 実行し始めたプロセスをlabelを付けて記憶します
        self.processes[label] = p

    def _process_target(self, command):
        '''
        Process用にメソッドを分割しました、必要無いかもしれません (self.c_auth.runを直接実行する)
        '''
        self.c_auth.run(command)

    def stop(self, label):
        '''
        バックグラウンドで実行しているプロセスを停止します
        '''

        if label not in self.processes:
            raise RuntimeError('label "{}" doesn\'t existed')
        p = self.processes.pop(label)
        p.terminate()
        p.join()

    def _add_command_prefix(self, command, sudo):
        '''
        コマンドにプレフィックス(sudoなど)を付けます、self.runから呼ばれる想定です。
        '''
        if sudo:
            return 'sudo ' + command
        return command

    def build(self, command, options):
        '''
        コマンドを生成します
        '''
        with open(COMMAND_YAML_DIR / Path(command)) as rf:
            data = yaml.load(rf)

        # 必須パラメータがあることを確認します
        for r in data['require']:
            if r not in options:
                raise RuntimeError('{} is not in options'.format(r))

        # yamlのoptionsを上から走査して、もし引数で値を受け取っていれば該当するパラメータをコマンドに加えます
        cmd = ''
        for r in data['options']:
            key, value = r.popitem()
            if options[key] is not None:
                cmd += ' ' + value.format(options[key])

        cmd = data['command'] + cmd

        return cmd

    def ping(self, destination, count=None, size=None, pmtudisc=None, interface=None, timeout=None):
        '''
        pingをフォアグラウンドで実行します
        引数はそのままself.buildで渡るため、kwargsを使用しても良いかもしれません
        しかし引数を正確に記載することで、ドキュメントを生成した際に受け取る引数が明示的になる利点も考えられます。
        '''
        cmd = self.build('ping.yaml', {'destination': destination, 'count': count, 'size': size,
                                       'pmtudisc': pmtudisc, 'interface': interface, 'timeout': timeout})

        result = self.run(cmd)
        # parserで結果を辞書化します
        result = parse('ping', result.stdout)
        return result

    def tcpdump(self, label, count=None, interface=None, module=None, readfile=None, snaplen=None, type=None, writefile=None, expression=None):
        '''
        tcpdumpをバックグラウンドで実行します
        '''
        cmd = self.build('tcpdump.yaml', {'count': count, 'interface': interface, 'module': module,
                                          'readfile': readfile, 'snaplen': snaplen, 'type': type, 'writefile': writefile, 'expression': expression})
        self.run_background(label, cmd, sudo=True)
