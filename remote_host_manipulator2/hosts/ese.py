import logging
import invoke

from remote_host_manipulator2.hosts.linux import Linux


class ESE(Linux):
    # Linuxを継承しつつ、__init__の引数はnamespaceが以外不要になります (良い継承ではないですが...)
    def __init__(self, namespace):

        logging.basicConfig()
        self.logger = logging.getLogger(
            "remote_host_manipulator." + self.__class__.__name__)
        self.logger.setLevel(logging.INFO)

        self.namespace = namespace
        # {label: Process()}
        self.processes = {}

    def _add_command_prefix(self, command, sudo):
        # 定数は実際の実装時は必要に応じて切り出します
        prefix = "sudo -E ip netns exec {} ssh -p 22 -l eseuser -i /home/auto-test/ese.key -o 'UserKnownHostsFile=/dev/null' -o 'StrictHostKeyChecking no' -o 'LogLevel QUIET' 192.0.2.253 ".format(self.namespace)
        return prefix + command

    def run(self, command, sudo=False):
        command = self._add_command_prefix(command, sudo)
        self.logger.info(command)
        result = invoke.run(command, hide=True)

        self.logger.info(result)
        return result

    def _process_target(self, command):
        self.invoke.run(command, hyde=True)
