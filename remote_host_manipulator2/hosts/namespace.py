from remote_host_manipulator2.hosts.linux import Linux


class Namespace(Linux):
    # Linuxを継承しつつ、__init__の引数にnamespaceが必須になります (良い継承の方法ではないですが....)
    def __init__(self, host, user, password, namespace, port=22):
        super().__init__(host, user, password, port)
        self.namespace = namespace

    def _add_command_prefix(self, command, sudo):
        prefix = 'sudo ip netns exec {} '.format(self.namespace)
        return prefix + command
