import textfsm.clitable as clitable
from pathlib import Path

INDEX_DIR = Path(__file__).parent
TEMPLATE_DIR = Path(__file__).parent / 'templates'


def parse(command, output):
    '''
    cisco_ios_maniopulatorと同様に、textfsmを利用してコマンドの結果を辞書にします
    しかし、コマンドによっては戻り値の型が配列であることも考えられるため、必要に応じてコマンド毎にparseメソッドを作成する必要がある可能性があります

    たとえば、pingの戻り値は辞書になるかと思いますが、ip routeの結果は配列のほうが望ましい可能性があります
    '''
    # template_path
    cli_table = clitable.CliTable(INDEX_DIR / Path("index"), TEMPLATE_DIR)
    attr = {"command": command}
    cli_table.ParseCmd(output, attr)

    ret_dict = {}

    for row in cli_table:
        for index, element in enumerate(row):
            ret_dict[cli_table.header[index]] = element

    return ret_dict
