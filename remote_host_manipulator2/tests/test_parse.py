from remote_host_manipulator2.parser.parse import parse


def test_parse_ping():
    PING_RESULT = """PING 8.8.8.8 (8.8.8.8) 64(92) bytes of data.
72 bytes from 8.8.8.8: icmp_seq=1 ttl=56 time=2.97 ms
72 bytes from 8.8.8.8: icmp_seq=2 ttl=56 time=2.93 ms
72 bytes from 8.8.8.8: icmp_seq=3 ttl=56 time=3.51 ms
72 bytes from 8.8.8.8: icmp_seq=4 ttl=56 time=3.30 ms
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3003ms
rtt min/avg/max/mdev = 2.938/3.182/3.518/0.240 ms
    """

    ret = parse('ping', PING_RESULT)
    assert ret == {
        'Destination': '8.8.8.8',
        'PacketLossRate': '0',
        'PacketsReceived': '4',
        'PacketsTransmitted': '4',
        'RTTAverage': '3.182',
        'RTTMaximum': '3.518',
        'RTTMeanDeviation': '0.240',
        'RTTMinimum': '2.938',
        'Sequences': ['72 bytes from 8.8.8.8: icmp_seq=1 ttl=56 time=2.97 ms',
                      '72 bytes from 8.8.8.8: icmp_seq=2 ttl=56 time=2.93 ms',
                      '72 bytes from 8.8.8.8: icmp_seq=3 ttl=56 time=3.51 ms',
                      '72 bytes from 8.8.8.8: icmp_seq=4 ttl=56 time=3.30 ms'],
        'Size': '64',
        'TotalTime': '3003'
    }
