from remote_host_manipulator2.hosts.namespace import Namespace
import pytest
from invoke.runners import Result
from unittest.mock import patch

DUMMY_HOST = '10.100.1.117'
DUMMY_PORT = 22
DUMMY_USER = 'kuwajimas'
DUMMY_PASSWORD = 'intellilink@01'

DUMMY_STDOUT = 'dummy stdout'
DUMMY_STDERR = 'dummy stderr'
DUMMY_RESULT = Result(stdout=DUMMY_STDOUT, stderr=DUMMY_STDERR)


@patch('fabric.Connection.run')
@pytest.mark.parametrize("command, namespace, expected_command", [
    ('dummy command op1 op2', 'dummy_namespace', 'sudo ip netns exec dummy_namespace dummy command op1 op2')
])
def test_run(mock_fabric_run, command, namespace, expected_command):
    ns = Namespace(host=DUMMY_HOST, port=DUMMY_PORT,
                   user=DUMMY_USER, password=DUMMY_PASSWORD, namespace=namespace)
    ns.run(command)

    # 意図したコマンドが生成され、fabricに渡されること
    created_cmd = mock_fabric_run.call_args[0][0]
    assert created_cmd == expected_command
