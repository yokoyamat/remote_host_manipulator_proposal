from remote_host_manipulator2.hosts.linux import Linux
import pytest
from invoke.runners import Result
from unittest.mock import patch

DUMMY_HOST = '10.100.1.117'
DUMMY_PORT = 22
DUMMY_USER = 'kuwajimas'
DUMMY_PASSWORD = 'intellilink@01'

DUMMY_STDOUT = 'dummy stdout'
DUMMY_STDERR = 'dummy stderr'
DUMMY_RESULT = Result(stdout=DUMMY_STDOUT, stderr=DUMMY_STDERR)


@patch('fabric.Connection.run')
@pytest.mark.parametrize("command, options, expected_command", [
    ('dummy command op1 op2', {}, 'dummy command op1 op2'),
    ('dummy command op1 op2', {'sudo': True}, 'sudo dummy command op1 op2'),
])
def test_run(mock_fabric_run, command, options, expected_command):
    linux = Linux(host=DUMMY_HOST, port=DUMMY_PORT,
                  user=DUMMY_USER, password=DUMMY_PASSWORD)
    linux.run(command, **options)

    # 意図したコマンドが生成され、fabricに渡されること
    created_cmd = mock_fabric_run.call_args[0][0]
    assert created_cmd == expected_command


@patch('remote_host_manipulator2.hosts.linux.Linux.run', return_value=DUMMY_RESULT)
@patch('remote_host_manipulator2.hosts.linux.parse')
@pytest.mark.parametrize("ping_options, expected_command", [
    ({"count": 4, "size": 64, "destination": "8.8.8.8"}, "ping -c 4 -s 64 8.8.8.8")
])
def test_ping(mock_parse, mock_client_run, ping_options, expected_command):
    client = Linux(host=DUMMY_HOST, port=DUMMY_PORT,
                   user=DUMMY_USER, password=DUMMY_PASSWORD)
    client.ping(**ping_options)

    # 意図したコマンドが生成されること
    created_cmd = mock_client_run.call_args[0][0]
    assert created_cmd == expected_command

    # コマンドをClient.run()で実行し、結果がparseに渡されること
    parse_string = mock_parse.call_args[0][0]
    assert parse_string == DUMMY_STDOUT


@patch('remote_host_manipulator2.hosts.linux.Linux.run_background', return_value=DUMMY_RESULT)
@pytest.mark.parametrize("label, options, expected", [
    ('label1', {}, "tcpdump")
])
def test_tcpdump(mock_result, label, options, expected):
    client = Linux(host="10.100.1.117", port=22,
                   user="kuwajimas", password="intellilink@01")

    client.tcpdump(label, **options)

    # 意図したラベルがつくこと
    labeled = mock_result.call_args[0][0]
    assert label == labeled

    # 意図したコマンドが生成されること
    created_cmd = mock_result.call_args[0][1]
    assert created_cmd == expected
