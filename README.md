# remote_host_manipulator

プロポーザル用のリポジトリなので全てのコメントは日本語で記載します。
この実装は、cloudwan-testingsの `IPoE環境のラフ実装デザインPR` で述べられた、「 `出来れば接続先のインスタンス生成を以下のファイルで行って、コマンドの実行先をインスタンス名で見分けたい（ただしそういった実装は現状無い）` 」の実装案になります。

https://bitbucket.org/LONDOBELL/cloudwan-testings/pull-requests/779#Ltests/21_IPoE/test_cases.robotT9

この形式で実装すると、RobotFrameworkからは以下のようにLinuxホストを操作できるため、操作対象のホストがわかりやすくなることが期待されます。

```
Library  remote_host_manipulator.hosts.linux.Linux  host=192.168.254.51  user=auto-test  password=nttpc123  WITH NAME  EMU1
Library  remote_host_manipulator.hosts.namespace.Namespace  host=192.168.254.11  user=auto-test  password=nttpc123  namespace=ns3035  WITH NAME  CLIENT1
Library  remote_host_manipulator.hosts.namespace.ESE  namespace=ns3035  WITH NAME  ESE_A

---

`${command_result}  CLIENT1.ping  destination=8.8.8.8  count=4  size=64
```

例としてpingのフォアグラウンド実行と、tcpdumpのバックグラウンド実行を実装しました。

## アーキテクチャ

4つの要素から構成されます。

### hosts

コマンドを実行するホストのクラス群です。remote_host_manipulatorは基本的に `hosts/` 以下のクラスをimportして使用します。
他の要素( `commands` ・ `parser` )は `host` から使用されます。
`Linux` では、 `commands/` 以下のyamlを使用したコマンドの生成・コマンド実行・バックグラウンドでのコマンド実行を実装しています。
`ESE` や `Namespace` は `Linux` を継承しており、コマンドを実行する上での差分や、 `ESE` / `Namespace` 上でしか実行できないコマンドを実装します。

元のremote_host_manipulatorと比べた時に、ホスト毎のコマンドのプレフィックス付与や、バックグランドでの実行が集約されるため、変更が容易になります。

### commands

コマンドを生成する際に参照される、コマンドに応じたオプションが記載されているyaml群です。
コマンドを実行する際、`host` はこのyamlを参照して、与えられた引数に応じたパラメータを持ったコマンドを生成します。
yamlにコマンドの生成ルールを記載して、コマンド生成部分 ( `Linux.build()` )をどのコマンドでも使い回すことで、コマンド生成部分の実装が簡潔になることが期待されます。

### parser

cisco_ios_manipulatorと同様の textfsm を使用したコマンド結果のパーサです。
textfsmを使うことで、元のremote_host_manipulatorよりもパーサの実装が簡潔になることが期待されます。
textfsmはDict in Dictのネスト構造を表現できないため、元のremote_host_manipulatorのパーサと異なった出力になります。

### tests

`hosts` / `parser` のテストです。 `commands` はyaml群のため、 `hosts` 内でテストします。

## クラス図(のようなもの)

![クラス図](https://bitbucket.org/yokoyamat/remote_host_manipulator_proposal/downloads/class.png)

### 簡易版

![簡易構成図](https://bitbucket.org/yokoyamat/remote_host_manipulator_proposal/downloads/%E7%B0%A1%E6%98%93%E6%A7%8B%E6%88%90%E5%9B%B3.jpg)
